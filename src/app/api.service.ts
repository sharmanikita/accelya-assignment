import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';


@Injectable()
export class ApiService {

  /**
   * Constructor
   */
  constructor(
    private httpClient: HttpClient,
  ) {

  }

  /**
   * Send a GET request to the API and returns an observable of a response
   */
  get<Request, Response>(endpoint: string, request?: Request, params?: any) {

    // Send data
    return this
      .httpClient
      .get<Response>(this.getHttpUrl(endpoint, params), {
        headers: this.getHttpHeaders(),
        params: this.getHttpParams<Request>(request),
      })
      .pipe(
        catchError((error) => this.onError(error)),
      );
  }

  /**
   * Return HTTP headers for every API request
   */
  private getHttpHeaders(): HttpHeaders {

    let headers = new HttpHeaders();

    headers = headers.append('X-Requested-With', 'XMLHttpRequest');

    return headers;
  }

  /**
   * Return HTTP params for the given request payload
   */
  private getHttpParams<Request>(request: Request): HttpParams {

    const params = {};

    if (request) {

      // Keep params that have a value, sort by key, then set params
      Object
        .keys(request)
        .filter(key => Array.isArray(request[key]) ? request[key].length > 0 : !!request[key])
        .sort()
        .forEach(key => params[key] = request[key]);
    }

    return new HttpParams({
      fromObject: params,
    });
  }

  /**
   * Return complete URL with injected params (example: replaces placeholder '{id}' by params.id)
   */
  private getHttpUrl(endpoint: string, params?: any): string {

    // Request URL
    let url = endpoint;

    // Data to inject into endpoint string
    if (params) {

      Object
        .keys(params)
        .forEach(key => {

          url = url.replace('{' + key + '}', params[key]);
        });
    }

    return url;
  }

  /**
   * Error response from API
   */
  private onError(error: any): Observable<never> {

    // TODO[later]: shady last condition
    if ((error.status === 401 || error.status === 403) && (window.location.href.match(/\?/g) || []).length < 2) {

      // TODO[later] ApiService should not redirect! Please emit an event, AppComponent can decide to redirect or not
      window.location.href = (error._body && (<{ login_url: string }>JSON.parse(error._body)).login_url);
    }

    return throwError(error);
  }
}
