import { Injectable } from '@angular/core';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  constructor(private apiService: ApiService) { }

  searchGifs(params) {
    
    return this.apiService.get('http://api.giphy.com/v1/gifs/search', params);
  }
}
