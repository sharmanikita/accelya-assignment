import {Component, OnInit} from '@angular/core';
import {GifsService} from '../gifs.service';

@Component({
  selector: 'app-gifs',
  templateUrl: './gifs.component.html',
  styleUrls: ['./gifs.component.css']
})
export class GifsComponent implements OnInit {

  searchQuery: string = '';
  gifs: Array<any> = [];
  cache: any = {};

  constructor(private gifsService: GifsService) {
  }

  ngOnInit() {
  }

  loadGifs(query) {
    const q = query['value'];
    if (this.cache[q]) {
      this.gifs = this.cache[q];
      return;
    }

    this.gifsService.searchGifs({
      q: q,
      api_key: 'sxSSTvCnao4Txb3zG4Bm2KzzMXhdrPaO'
    }).subscribe(response => {
      this.cache[q] = response['data'];
      this.gifs = response['data'];
    });
  }

}
